const fetch = require('node-fetch');
const app   = require('express')();
const b64   = require('nodejs-base64-encode');

require('dotenv').config();

let url = process.env.ENDPOINT;

console.clear(); console.log('\n\n');

const client  = process.env.CLIENT || "";
const secret  = process.env.SECRET || "";
const encoded = b64.encode(`${client}:${secret}`, 'base64');

console.log({
   client,
   secret,
   encoded
});

const jsonBody = {
   client   : client,
   secret   : secret,
   
   user     : client,
   password : secret,
   
   msg      : "test msg"
};

console.log(`\nService URL: ${url}`);

fetch(url, {
   method: 'POST',
   headers: {
      "Authorization": 'Basic ' + encoded,
      "Content-Type" : 'application/json',
   },
   "body": JSON.stringify( jsonBody )
})
.then(rslt => rslt.text())
.then(resp => console.log({resp}))
.catch(err => console.log({err}))

app.listen(8880, '10.8.1.26', () => {
   console.log('\nServidor iniciado en http://10.8.1.26:8880\n');
});

