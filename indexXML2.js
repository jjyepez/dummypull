const fetch = require('node-fetch');
const app   = require('express')();

require('dotenv').config();

let url = process.env.ENDPOINT;

console.clear(); console.log('\n\n');

const client = process.env.CLIENT || "";
const secret = process.env.SECRET || "";
const ani    = process.env.ANI    || "";

console.log({
   client,
   secret,
   ani
});

let xmlBody = `
  <?xml version="1.0" encoding="UTF-8"?>
  <!DOCTYPE svc_init SYSTEM "Mlp_svc_init_320.dtd">
  <svc_init ver="3.2.0">
    <hdr ver="3.2.0">
      <client>
        <id>${client}</id>
        <pwd>${secret}</pwd>
      </client>
    </hdr>
    <eme_lir res_type="SYNC" ver="3.2.0">
      <msids>
        <msid enc="ASC" type="MSISDN">${ani}</msid>
      </msids>
    </eme_lir>
  </svc_init>
`;
console.log(xmlBody);
xmlBody = xmlBody.replace(/[\n\t\s]*/ig,'');

const net    = require('net');
const TCPclient = new net.Socket();

let port = process.env.PORT || 9210;
let host = process.env.HOST || '10.67.36.134';

console.log(`\nService: ${host}:${port}`);

TCPclient.connect(port, host, function() {
	console.log('Connected');

	// console.log('Petición:', xmlBody);
	TCPclient.write(xmlBody);
});

TCPclient.on('data', function(data) {
	console.log('Received: ' + data);
	TCPclient.destroy(); // kill client after server's response
});

TCPclient.on('close', function() {
	console.log('Connection closed');
});

app.listen(8880, '10.8.1.26', () => {
   console.log('\nServidor iniciado en http://10.8.1.26:8880\n');
});

