const fetch = require('node-fetch');
const app   = require('express')();
const b64   = require('nodejs-base64-encode');

require('dotenv').config();

let url = process.env.ENDPOINT;

console.clear(); console.log('\n\n');

const client  = process.env.CLIENT || "";
const secret  = process.env.SECRET || "";
const ani     = process.env.ANI    || "";
const encoded = b64.encode(`${client}:${secret}`, 'base64');

console.log({
   client,
   secret,
   encoded,
   ani
});

let xmlBody = `<?xml version="1.0" encoding="UTF-8"?>
  <!DOCTYPE svc_init SYSTEM "Mlp_svc_init_320.dtd">
  <svc_init ver="3.2.0">
    <hdr ver="3.2.0">
      <client>
        <id>${client}</id>
        <pwd>${secret}</pwd>
      </client>
    </hdr>
    <eme_lir res_type="SYNC" ver="3.2.0">
      <msids>
        <msid enc="ASC" type="MSISDN">${ani}</msid>
      </msids>
    </eme_lir>
  </svc_init>
`;

console.log(xmlBody);
// xmlBody = xmlBody.replace(/[\n\t\s]*/ig,'');

console.log(`\nService URL: ${url}`);

fetch(url, {
   method: 'POST',
   headers: {
      "Content-Type" : 'text/xml',
   },
   "body": xmlBody
})
.then(rslt => rslt.text())
.then(resp => console.log({resp}))
.catch(err => console.log({err}))

app.listen(8880, '10.8.1.26', () => {
   console.log('\nServidor iniciado en http://10.8.1.26:8880\n');
});

